# ![Logo](https://user-images.githubusercontent.com/49928332/132255432-b84c3399-e488-43fa-8ccf-605ae146965f.png) BlockRename ![Logo](https://user-images.githubusercontent.com/49928332/132255432-b84c3399-e488-43fa-8ccf-605ae146965f.png)
Block and modify renames from anvil!

![Plugin in work](https://user-images.githubusercontent.com/49928332/132254855-0ad457a8-a24f-47db-af55-6ba4bcffba79.png)

## Download
You can download binary from [releases page](https://github.com/cofob/BlockRename/releases).

## Dependencies
  - Vault
  - Spigot

## Config
```yaml
block:
- some names
- to block here
permissions:
- cofob.blockrename.colored
- cofob.blockrename.bypass
- cofob.blockrename.reload
no_permission: '&4You don''''t have the permission to perform this action!'
blocked_text: 'BLOCKED!'  # The text to which the name of the subject will be replaced
```

### by cofob
